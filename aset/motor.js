console.log('motor.js');
showMotor(true);
var motorData;
var markerMotor = new Array();

function showMotor(stat){

	getMotor(function() {
		if (stat) {
			if(markerAdditional['motor'] != undefined) {
				removeAdditional(markerAdditional['motor']);
			}

			if (motorData != null && motorData.length > 0 ) 
			{
				// data from pg
				$.each(motorData, function(i, item){
					var lat = parseFloat(item.cur_lat);
					var lon = parseFloat(item.cur_lon);

					var labelItem = '<p align="center">Motor ' + item.merk +
									'<br/> Alamat: ' + item.no_polisi +'</p>';
					markerMotor[item.id_gsm] = new L.marker([item.cur_lat, item.cur_lon],
							{id_gsm: item.id_gsm})
						.bindPopup(labelItem)
						.on('click', function(){
							// request history data
							requestMotorHistory(this);
						})
						.addTo(map);
					// map.addLayer(markerMotor[item.id_gsm]);
						// drawnGeojson.addLayer(markerMotor[item.id_gsm]);
					// $("#feature-list tbody").append('<tr onclick="panto('+item.cur_lat+','+item.cur_lon+','+item.id_gsm+')" class="feature-row" id="' + item.id_motor + '"><td style="text-align: center; vertical-align: middle;"><img src="../aset/img/marker-icon.png" width="13" height="20""></td><td style="vertical-align: middle;" class="feature-name">' + item.merk + '</td><td class="feature-name">' + item.no_polisi + '</td><td style="vertical-align: middle;"><i class="fa fa-chevron-right pull-right"></i></td></tr>');
				});

				labelItem = null;
			}

			// stat_view.groundStation = true;
		}
		else {
			// stat_view.groundStation = false;
			// if (markerAdditional['motor']!=undefined) {
			// 	removeAdditional(markerAdditional['motor']);
			// }
			console.log('showMotor false ', markerMotor);
			markerMotor['abc'].setLatLng([-6.862948, 107.584018]);
		}
	});
}

socket.on('dataPosMotor', function(data) {
	console.log('dataPosMotor ', data);
	markerMotor[data.id_gsm].setLatLng([data.lat, data.lon]);
});

function getMotor(fn){
	socket.emit('reqMotor');
	socket.once('resMotor', function (data)
	{
		motorData = data;

		fn();
	});
}

function requestMotorHistory(thisObj) { 
	if (aispolyline) {
		map.removeLayer(aispolyline);
		aispolyline = null;
	}
	socket.emit('reqMotorHistory', {id_gsm: thisObj.options.id_gsm, limit : 100 });
}

var aispolyline = null;
socket.on('resMotorHistory', function (data) {
	var point = null;
	var pointList = [];
	var id_gsm;
	for (i = 0; i < data.length; i++) { 			
		point = new L.LatLng(data[i].lat, data[i].lon);
		pointList.push(point);
	}
	
	aispolyline = new L.polyline(pointList, {
		color: 'green',
		weight: 3,
		opacity: 1,
		smoothFactor: 1
	}); 

	var histLabel = 'Histori Motor utk id_gsm ' + id_gsm;
	aispolyline.bindPopup(histLabel, { noHide: true });

	map.addLayer(aispolyline);
});