<body>
	<div id="container">
		<ul id="side-menu">
			<li class="category">Kost Edumedia</li>
			<li id="logo">
			<img src="<?php echo base_url() ?>aset/img/1.png"  />
			</li>
<?php if ($permission) { ?>
			<li>
				<a href="<?php echo base_url() . 'admin/pemilik_ctrl' ?>" <?php if (isset($current_context) && $current_context == '/admin/pemilik_ctrl') echo 'class="current"' ?>>
					<i class='fa fa-user'></i>&nbsp;PEMILIK
				</a>
			</li>
			<li>
				<a href="<?php echo base_url() . 'admin/komplain_ctrl' ?>" <?php if (isset($current_context) && $current_context == '/admin/komplain_ctrl') echo 'class="current"' ?>>
					<i class='fa fa-home'></i>&nbsp;KOMPLAIN
				</a>
			</li>
<?php } ?>

		</ul>

		<div id="content">

			<div id="title-up">
				<?php if (isset($title)) echo $title ?>
				<a class="red" href="<?php echo base_url() . 'home/logout' ?>"><i class='fa fa-sign-out'></i>&nbsp;Keluar</a> 			
			</div>

			<div class="clear"></div>
			<br />
